package com.example.movie_android_mini_project.network;

import com.example.movie_android_mini_project.model.Genres;
import com.example.movie_android_mini_project.model.Movie;
import com.example.movie_android_mini_project.model.Movies;
import com.example.movie_android_mini_project.model.Reviews;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class RetrofitClientInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://api.themoviedb.org/";
    private static final String URL_PATH = "/3/";
    private static final String API_KEY = "fc69ae8afb318e3ffe8ed397dc523ae5";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public interface GetDataService {
        @GET(URL_PATH + "genre/movie/list?api_key=" + API_KEY + "&language=en-US")
        Call<Genres> getAllGenres();

        @GET(URL_PATH + "discover/movie?api_key=" + API_KEY + "&language=en-US&")
        Call<Movies> getMovies(@Query("with_genres") String genreId, @Query("page") String page);

        @GET(URL_PATH + "movie/{movieId}?api_key=" + API_KEY + "&language=en-US&append_to_response=videos")
        Call<Movie> getMovieDetails(
                @Path("movieId") int movieId
        );

        @GET(URL_PATH + "movie/{movieId}/reviews?api_key=" + API_KEY + "&language=en-US&")
        Call<Reviews> getReviews(
                @Path("movieId") int movieId,
                @Query("page") String page
        );
    }
}
