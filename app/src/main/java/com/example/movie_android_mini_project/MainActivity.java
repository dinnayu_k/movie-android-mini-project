package com.example.movie_android_mini_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.movie_android_mini_project.fragments.GenresFragment;

public class MainActivity extends AppCompatActivity {

    public TextView headerTitle;
    public ImageView backButton;
    public LinearLayout maskingView;
    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = new Bundle();
        setContentView(R.layout.activity_main);

        headerTitle = findViewById(R.id.header);
        backButton = findViewById(R.id.back_button);
        maskingView = findViewById(R.id.masking);
        progressBar = findViewById(R.id.progress_bar);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().popBackStack();
            }
        });

        GenresFragment fragment = new GenresFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.add(R.id.activity_content, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setHeaderTitle(String title){
        headerTitle.setText(title);
    }

    public void showHideBackButton(boolean isShow){
        if (isShow){
            backButton.setVisibility(View.VISIBLE);
        } else {
            backButton.setVisibility(View.GONE);
        }
    }

    public void showHideMaskingOverlay(boolean isShow){
        if (isShow){
            maskingView.setVisibility(View.VISIBLE);
        } else {
            maskingView.setVisibility(View.GONE);
        }
    }
}
