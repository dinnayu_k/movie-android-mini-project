package com.example.movie_android_mini_project.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Movies {

    public int page;
    @SerializedName("total_results")
    public int totalPages;
    @SerializedName("total_pages")
    public int totalResults;
    public List<Movie> results;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
