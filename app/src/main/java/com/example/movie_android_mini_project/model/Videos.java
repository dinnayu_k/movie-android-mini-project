package com.example.movie_android_mini_project.model;

import java.util.List;

public class Videos {

    List<Video> results;

    public List<Video> getResults() {
        return results;
    }

    public void setResults(List<Video> results) {
        this.results = results;
    }
}
