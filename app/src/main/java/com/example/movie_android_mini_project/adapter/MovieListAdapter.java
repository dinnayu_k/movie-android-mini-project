package com.example.movie_android_mini_project.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.movie_android_mini_project.MainActivity;
import com.example.movie_android_mini_project.R;
import com.example.movie_android_mini_project.common.Constant;
import com.example.movie_android_mini_project.model.Movie;

import java.util.List;

public class MovieListAdapter  extends RecyclerView.Adapter<MovieListAdapter.MovieViewHolder> {

    MovieOnClickListener listener;

    private List<Movie> movieList;
    private Context mContext;

    public void setMovieListener(MovieOnClickListener listener){
        this.listener = listener;
    }

    public MovieListAdapter(Context mContext, List<Movie> movieList){
        this.mContext = mContext;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.movie_row, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, final int position) {
        holder.movieRating.setText(String.valueOf(movieList.get(position).getVoteAverage()));
        holder.movieTitle.setText(movieList.get(position).getTitle());
        String imagePath = movieList.get(position).getPosterPath();
        if (!imagePath.isEmpty()){

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher_round)
                    .error(R.mipmap.ic_launcher_round);

            Glide.with(mContext)
                    .load(Constant.IMAGE_URL+imagePath)
                    .apply(options)
                    .into(holder.moviePoster);
        }
        holder.movieReleaseDate.setText(movieList.get(position).getReleaseDate());
        holder.movieOverview.setText(movieList.get(position).getOverview());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null){
                    listener.onClickMovie(movieList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        ImageView moviePoster;
        TextView movieTitle;
        TextView movieRating;
        TextView movieReleaseDate;
        TextView movieOverview;

        MovieViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            moviePoster = mView.findViewById(R.id.movie_poster_image);
            movieTitle = mView.findViewById(R.id.movie_title);
            movieRating = mView.findViewById(R.id.movie_rating);
            movieReleaseDate = mView.findViewById(R.id.movie_release_date);
            movieOverview = mView.findViewById(R.id.movie_overview);
        }
    }

    public interface MovieOnClickListener{
        void onClickMovie(Movie movie);
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            ((MainActivity) context).showHideMaskingOverlay(true);
        }
    }
}
