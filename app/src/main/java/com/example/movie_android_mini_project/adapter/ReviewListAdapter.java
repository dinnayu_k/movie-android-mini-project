package com.example.movie_android_mini_project.adapter;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movie_android_mini_project.R;
import com.example.movie_android_mini_project.model.Review;

import java.util.List;


public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ReviewVH> {

    private List<Review> reviews;

    public ReviewListAdapter(List<Review> reviews){
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ReviewVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.review_row, parent, false);
        return new ReviewVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewVH holder, int position) {
        holder.reviewAuthor.setText(reviews.get(position).getAuthor());
        holder.reviewContent.setText(reviews.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    class ReviewVH extends RecyclerView.ViewHolder {
        public final View mView;
        TextView reviewAuthor;
        TextView reviewContent;

        ReviewVH(View itemView) {
            super(itemView);
            mView = itemView;
            reviewAuthor = mView.findViewById(R.id.review_author);
            reviewContent = mView.findViewById(R.id.review_content);
        }
    }

}
