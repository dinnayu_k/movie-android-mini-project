package com.example.movie_android_mini_project.common;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.movie_android_mini_project.MainActivity;
import com.example.movie_android_mini_project.R;

public class CommonUtils {

    public static void setHeader(FragmentActivity activity, String title, boolean isShowBackButton){
        ((MainActivity) activity).setHeaderTitle(title);
        ((MainActivity) activity).showHideBackButton(isShowBackButton);
    }

    public static void navigateToPage(FragmentActivity activity, Fragment fragment, Bundle bundle){
        FragmentManager fragmentManager = activity.getSupportFragmentManager() ;
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.activity_content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
