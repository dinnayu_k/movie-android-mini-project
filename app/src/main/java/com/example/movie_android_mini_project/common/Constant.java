package com.example.movie_android_mini_project.common;

public class Constant {
    public static final String GENRE_ID_KEY = "GENRE_ID";
    public static final String HEADER_ID_KEY = "HEADER_TITLE";
    public static final String MOVIE_ID_KEY = "MOVIE_ID";
    public static String IMAGE_URL = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/";

    public static final String ERROR_MESSAGE = "Something went wrong...Please try later!";
    public static final String HOME = "HOME";
}
