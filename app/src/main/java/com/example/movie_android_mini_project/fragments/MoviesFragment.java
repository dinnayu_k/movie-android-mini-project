package com.example.movie_android_mini_project.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.movie_android_mini_project.MainActivity;
import com.example.movie_android_mini_project.R;
import com.example.movie_android_mini_project.adapter.MovieListAdapter;
import com.example.movie_android_mini_project.common.CommonUtils;
import com.example.movie_android_mini_project.common.Constant;
import com.example.movie_android_mini_project.model.Movie;
import com.example.movie_android_mini_project.model.Movies;
import com.example.movie_android_mini_project.network.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesFragment extends Fragment implements MovieListAdapter.MovieOnClickListener {

    private Bundle bundle;
    private RecyclerView mRecyclerView;
    private MovieListAdapter mAdapter;
    private List<Movie> movieList = new ArrayList<>();
    private int page = 1;
    private int totalPages;
    private String genreId;

    public MoviesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bundle != null){
            String headerTitle = bundle.getString(Constant.HEADER_ID_KEY);
            CommonUtils.setHeader(getActivity(), headerTitle, true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bundle = getArguments();

        View view = inflater.inflate(R.layout.fragment_movies, container, false);

        mRecyclerView = view.findViewById(R.id.movie_recyclerView);

        setAdapters();

        if (bundle != null){
            genreId = bundle.getString(Constant.GENRE_ID_KEY);
            requestMovies();
        }

        // Inflate the layout for this fragment
        return view;
    }

    /*Method to initilize adapters*/
    private void setAdapters() {
        mAdapter = new MovieListAdapter(getContext(),movieList);
        mAdapter.setMovieListener(this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        // Implement endless scroll
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount){
                        page += 1;
                        if (page <= totalPages && page <= 1000 ){
                            requestMovies();
                        }
                    }

            }}
        });
    }

    //  Method to call get movies service
    private void requestMovies(){
        /*Call services to get movies by genre*/
        ((MainActivity) getActivity()).showHideMaskingOverlay(true);
        RetrofitClientInstance.GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(RetrofitClientInstance.GetDataService.class);
        Call<Movies> call = service.getMovies(genreId, String.valueOf(page));
        call.enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Call<Movies> call, Response<Movies> response) {
                ((MainActivity) getActivity()).showHideMaskingOverlay(false);
                if (response.body() != null){
                    if (totalPages == 0){
                        totalPages = response.body().totalPages;
                    }
                    movieList.addAll(response.body().getResults());
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Movies> call, Throwable t) {
                ((MainActivity) getActivity()).showHideMaskingOverlay(false);
                showErrorDialog();
            }
        });
    }

    // This method is triggered when user click on one of movie
    @Override
    public void onClickMovie(Movie movie) {
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constant.HEADER_ID_KEY, movie.getTitle());
        bundle.putInt(Constant.MOVIE_ID_KEY, movie.getId());
        fragment.setArguments(bundle);
        CommonUtils.navigateToPage(getActivity(), fragment, bundle);
    }

    // Method to show error dialog
    public void showErrorDialog(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(Constant.ERROR_MESSAGE);
        dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                requestMovies();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }
}
