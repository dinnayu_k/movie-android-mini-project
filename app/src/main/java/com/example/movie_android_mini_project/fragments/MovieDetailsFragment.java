package com.example.movie_android_mini_project.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.movie_android_mini_project.MainActivity;
import com.example.movie_android_mini_project.R;
import com.example.movie_android_mini_project.adapter.ReviewListAdapter;
import com.example.movie_android_mini_project.common.CommonUtils;
import com.example.movie_android_mini_project.common.Constant;
import com.example.movie_android_mini_project.model.Movie;
import com.example.movie_android_mini_project.model.Review;
import com.example.movie_android_mini_project.model.Reviews;
import com.example.movie_android_mini_project.network.RetrofitClientInstance;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailsFragment extends Fragment {

    private Bundle bundle;
    private ImageView movieImage;
    private TextView movieTitle;
    private TextView movieReleaseYear;
    private TextView movieOverview;
    private TextView overviewText;
    private TextView movieRating;
    private TextView reviewText;
    private RelativeLayout ratingPlayView;
    private RecyclerView reviewRecyclerView;

    private ReviewListAdapter mReviewAdapter;
    private List<Review> reviewList = new ArrayList<>();
    private int page = 1;
    private int movieId, totalPages;
    private boolean isShowErrorDialog;

    public MovieDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bundle != null){
            String headerTitle = bundle.getString(Constant.HEADER_ID_KEY);
            CommonUtils.setHeader(getActivity(), headerTitle, true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie_details, container, false);
        movieImage = view.findViewById(R.id.movie_detail_poster);
        movieTitle = view.findViewById(R.id.movie_detail_title);
        movieReleaseYear = view.findViewById(R.id.movie_detail_year);
        movieOverview = view.findViewById(R.id.movie_detail_overview);
        overviewText = view.findViewById(R.id.overview_text);
        movieRating = view.findViewById(R.id.movie_detail_rating);
        ratingPlayView = view.findViewById(R.id.ratin_play_view);
        reviewRecyclerView = view.findViewById(R.id.review_recycler_view);
        reviewText = view.findViewById(R.id.reviews_text);

        setAdapters();

        bundle = getArguments();
        if (bundle != null) {
            movieId = bundle.getInt(Constant.MOVIE_ID_KEY);
            /*Call services to get movies by genre*/
            getMovieDetails();
            getReviews();
        }

        return view;
    }

    // Method to get movie details
    private void getMovieDetails(){
        ((MainActivity) getActivity()).showHideMaskingOverlay(true);
            RetrofitClientInstance.GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(RetrofitClientInstance.GetDataService.class);
            Call<Movie> call = service.getMovieDetails(movieId);
            call.enqueue(new Callback<Movie>() {
                @Override
                public void onResponse(Call<Movie> call, Response<Movie> response) {
                    ((MainActivity) getActivity()).showHideMaskingOverlay(false);
                    if (response.body() != null){
                        Movie movie = response.body();

                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.mipmap.ic_launcher_round)
                                .error(R.mipmap.ic_launcher_round);
                        if (!response.body().getPosterPath().isEmpty()){
                            Glide.with(getContext())
                                    .load(Constant.IMAGE_URL+movie.getPosterPath())
                                    .apply(options)
                                    .into(movieImage);
                        }

                        movieTitle.setText(movie.getTitle());

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date parse = null;
                        try {
                            parse = sdf.parse(movie.getReleaseDate());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Calendar c = Calendar.getInstance();
                        c.setTime(parse);

                        movieReleaseYear.setText("(" + String.valueOf(c.get(Calendar.YEAR)) + ")");
                        movieOverview.setText(movie.getOverview());
                        movieRating.setText(String.valueOf(movie.getVoteAverage()));
                        overviewText.setVisibility(View.VISIBLE);
                        ratingPlayView.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<Movie> call, Throwable t) {
                    ((MainActivity) getActivity()).showHideMaskingOverlay(false);
                    showErrorDialog();
                }
            });
    }

    //Method to get reviews
    public void getReviews(){
        ((MainActivity) getActivity()).showHideMaskingOverlay(true);
        RetrofitClientInstance.GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(RetrofitClientInstance.GetDataService.class);
        Call<Reviews> call = service.getReviews(movieId, String.valueOf(page));
        call.enqueue(new Callback<Reviews>() {
            @Override
            public void onResponse(Call<Reviews> call, Response<Reviews> response) {
                if (response.body() != null){
                    Reviews reviews = response.body();

                    if (reviews != null) {
                        if (totalPages == 0){
                            totalPages = reviews.getTotalPages();
                        }

                        String text = reviews.getResults().size() != 1 ? "Reviews (" + String.valueOf(reviews.getResults().size()) + ")" : "Review (1)";
                        reviewText.setText(text);
                        reviewText.setVisibility(View.VISIBLE);
                        reviewList.addAll(reviews.getResults());
                        mReviewAdapter.notifyDataSetChanged();
                    }
                }

                ((MainActivity) getActivity()).showHideMaskingOverlay(false);
            }

            @Override
            public void onFailure(Call<Reviews> call, Throwable t) {
                ((MainActivity) getActivity()).showHideMaskingOverlay(false);
                showErrorDialog();
            }
        });
    }

    /*Method to initialize adapters*/
    private void setAdapters() {
        mReviewAdapter = new ReviewListAdapter(reviewList);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        reviewRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(reviewRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        reviewRecyclerView.addItemDecoration(horizontalDecoration);
        reviewRecyclerView.setAdapter(mReviewAdapter);

        // Implement endless scroll
        reviewRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount){
                        page += 1;
                        if (page <= totalPages && page <= 1000 ){
                            getReviews();
                        }
                    }

                }}
        });
    }

    // Method to show error dialog
    public void showErrorDialog(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(Constant.ERROR_MESSAGE);
        dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                isShowErrorDialog = false;
                dialogInterface.dismiss();
            }
        });
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                isShowErrorDialog = false;
                dialogInterface.dismiss();
                getReviews();
                getMovieDetails();
            }
        });
        dialog.setCancelable(false);
        if (!isShowErrorDialog){
            isShowErrorDialog = true;
            dialog.show();
        }

    }
}
