package com.example.movie_android_mini_project.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.movie_android_mini_project.MainActivity;
import com.example.movie_android_mini_project.R;
import com.example.movie_android_mini_project.adapter.GenreListAdapter;
import com.example.movie_android_mini_project.common.CommonUtils;
import com.example.movie_android_mini_project.common.Constant;
import com.example.movie_android_mini_project.model.Genre;
import com.example.movie_android_mini_project.model.Genres;
import com.example.movie_android_mini_project.network.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenresFragment extends Fragment implements GenreListAdapter.GenreOnClickListener {

    private GenreListAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private List<Genre> genreList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        CommonUtils.setHeader(getActivity(), "", false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_genres, container, false);
        mRecyclerView = view.findViewById(R.id.recyvlerView);
        getGenres();
        setmAdapters();

        return view;
    }

    // Call genres from service vall
    public void getGenres(){
        ((MainActivity) getActivity()).showHideMaskingOverlay(true);
        /*Call services to get genrese*/
        RetrofitClientInstance.GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(RetrofitClientInstance.GetDataService.class);
        Call<Genres> call = service.getAllGenres();
        call.enqueue(new Callback<Genres>() {
            @Override
            public void onResponse(Call<Genres> call, Response<Genres> response) {
                ((MainActivity) getActivity()).showHideMaskingOverlay(false);
                genreList.addAll(response.body().getGenres());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Genres> call, Throwable t) {
                ((MainActivity) getActivity()).showHideMaskingOverlay(false);
                showErrorDialog();
            }
        });
    }

    /*Method to initialize adapters*/
    private void setmAdapters() {
        mAdapter = new GenreListAdapter(getContext(),genreList);
        mAdapter.setGenreListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(Genre genre) {
        MoviesFragment fragment = new MoviesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constant.HEADER_ID_KEY, genre.getName());
        bundle.putString(Constant.GENRE_ID_KEY, String.valueOf(genre.getId()));
        fragment.setArguments(bundle);
        CommonUtils.navigateToPage(getActivity(), fragment, bundle);
    }

    // Method to show error dialog
    public void showErrorDialog(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(Constant.ERROR_MESSAGE);
        dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                getGenres();
            }
        });
        dialog.setCancelable(false);
        dialog.show();

    }
}
